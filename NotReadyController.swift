//
//  NotReadyController.swift

import WatchKit

class NotReadyController: WKInterfaceController {
	
	override func awake(withContext context: Any?) {
		super.awake(withContext: context)
		
		MeasurementManager.instance.delegate = self
	}
	
	override func willActivate() {
		// This method is called when watch view controller is about to be visible to user
		super.willActivate()
		
		let isAppReady = UserDefaults.standard.bool(forKey: KeyIsAppReady)
		dLog(isAppReady)
		if isAppReady{
			WKInterfaceController.reloadRootControllers(withNames: ["start","second","third"], contexts: nil)
		}
	}
	
	override func didDeactivate() {
		// This method is called when watch view controller is no longer visible
		super.didDeactivate()
	}
}

extension NotReadyController:MeasurementDelegate{
	func onReady() {
		let isAppReady = UserDefaults.standard.bool(forKey: KeyIsAppReady)
		dLog(isAppReady)
		if isAppReady{
			WKInterfaceController.reloadRootControllers(withNames: ["start","second","third"], contexts: nil)
		}
	}
}
