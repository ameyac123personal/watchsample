//
//  MeasureController.swift

import WatchKit
import Foundation

class MeasureController: WKInterfaceController {
    
    @IBOutlet var measurementView: WKInterfaceGroup!
    @IBOutlet var savingView: WKInterfaceGroup!
    @IBOutlet var endView: WKInterfaceGroup!	
    @IBOutlet var pecentageView: WKInterfaceLabel!
    
    var isCompleted = false
    var isStopped = false
    
    @IBOutlet var surveyText: WKInterfaceLabel!
    
    override func awake(withContext context: Any?) {
        super.awake(withContext: context)
    }
    
    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
        dLog("willActivate")
        MeasurementManager.instance.delegate = self
        
        
        let isAppReady = UserDefaults.standard.bool(forKey: KeyIsAppReady)
        dLog(isAppReady)
        if !isAppReady{
            WKInterfaceController.reloadRootControllers(withNames: ["NotReadyController"], contexts: nil)
            return
        }
        
        if MeasurementManager.instance.startDate == nil && isCompleted{
            showEndView()
        }
        if let _ = UserDefaults.standard.string(forKey: KeySessionId) {
            surveyText.setText("Processing….\n \n Take a moment to complete your daily survey while you wait.")
        } else {
            surveyText.setText("Processing….\n \n")
        }
        
    }
    
    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
        dLog("didDeactivate")
        MeasurementManager.instance.delegate = nil
    }
    
    @IBAction func onTapStopButton() {
        let okButtonAction = WKAlertAction(title:"ok".localized, style: .default) { () -> Void in
            
            MeasurementManager.instance.stopMeasureItch()
            self.showConfirmMeasurementDialog()
            self.measurementView.setHidden(true)
        }
        
        let cancelButtonAction = WKAlertAction(title:"cancel".localized, style: .default) { () -> Void in
            
        }
        
        presentAlert(withTitle: nil, message:"stop_measurement_text".localized, preferredStyle: .alert, actions: [okButtonAction, cancelButtonAction]);
    }
    
    @IBAction func onTapEndButton() {
        
        isCompleted = true
        showFirstController()
    }
    
    func showFirstController(){
        WKInterfaceController.reloadRootControllers(withNames: ["start","second","third"], contexts: nil)
    }
    
    
    func showConfirmMeasurementDialog(){
        let okButtonAction = WKAlertAction(title:"ok".localized, style: .default) { () -> Void in
            
            self.showSavingView()
            
            MeasurementManager.instance.measureItch()
            self.isCompleted = true
        }
        
        let cancelButtonAction = WKAlertAction(title:"delete_measurement_data".localized, style: .default) { () -> Void in
            MeasurementManager.instance.reset()
            self.measurementView.setHidden(false)
            self.showFirstController()
        }
        
        let measuredInterval = MeasurementManager.instance.getMeasurementInterval()
        dLog(measuredInterval,measuredInterval.hour,measuredInterval.min,measuredInterval.sec)
        var interval:String!
        if 0 < measuredInterval.hour{
            interval = String(format: "measured_interval_hm".localized, measuredInterval.hour,measuredInterval.min)
        }else if(0 < measuredInterval.min){
            interval = String(format: "measured_interval_ms".localized, measuredInterval.min,measuredInterval.sec)
        }else{
            interval = String(format: "measured_interval_s".localized, measuredInterval.sec)
        }
        dLog(interval)
        
        let message = String(format:"confirm_stop_measurement_text".localized,interval)
        DispatchQueue.main.async {
            self.presentAlert(withTitle: nil, message:message, preferredStyle: .alert, actions: [okButtonAction, cancelButtonAction]);
        }
    }
    
    func showSavingView(){
        savingView.setHidden(false)
    }
    
    func showEndView(){
        
        savingView.setHidden(true)
        endView.setHidden(false)
    }
    
    func sendDataToiPhone(){
        
        DispatchQueue.main.async {
            self.showEndView()
        }
        
    }
}

extension MeasureController:MeasurementDelegate{
    
    func progress(_ percentage: Int) {
        
        //		dLog(percentage)
        DispatchQueue.main.async {
            self.pecentageView.setText(String(format: "saving_percentage".localized, percentage))
        }
    }
    
    func finish(){
        DispatchQueue.main.async {
            //			self.sendDataToiPhone()
            self.showEndView()
        }
        
    }
}
